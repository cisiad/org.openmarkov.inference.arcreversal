/*
 * Copyright 2013 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.arcReversal.action;

import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.inference.arcReversal.ArcReversal;

import java.util.List;
//import org.openmarkov.inference.arcReversal.ArcReversal.InferencePurpose;
//import org.openmarkov.inference.arcReversal.ArcReversal.InferenceState;


@SuppressWarnings("serial")
/** Removes a decision node in a variable elimination algorithm.
 * This Edit class is valid for Bayesian networks and influence diagrams.
 * */
public class CRemoveDecisionNodeAREdit extends CRemoveNodeAREdit {

	// InferencePurpose purpose, 
	//,
	//InferenceState inferenceState
	public CRemoveDecisionNodeAREdit(ProbNet probNet,
	                                 List<TablePotential> constantPotentials,
			Variable variableToDelete,
			ArcReversal arcReversal, boolean isLastVariable) {
		super(probNet, constantPotentials, variableToDelete,
				arcReversal, CRemoveDecisionNodeAREdit.class,
				isLastVariable); // purpose, , inferenceState

	}

	// InferencePurpose purpose,
	//,InferenceState inferenceState
    private CRemoveDecisionNodeAREdit(ProbNet probNet, List<TablePotential> constantPotentials,
                                      Variable variableToDelete, ArcReversal arcReversal) {
		this(probNet,constantPotentials,variableToDelete,arcReversal,false); // purpose,,inferenceState
	
	}

	@Override
	// Change this...
	protected TablePotential auxMarginalizePotentials(List<TablePotential> potentials) {	
		
		TablePotential[] newPotentials = DiscretePotentialOperations
			    .multiplyAndMaximizeUniformly(potentials, variable);
		
		TablePotential newPotential = newPotentials[0];
		if (DiscretePotentialOperations.isThereAUtilityPotential(potentials)){
			//Save the policy
			//policy = newPotentials[1].toString();
		}
		return newPotential;
	
	}
}
