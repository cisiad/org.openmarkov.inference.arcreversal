package org.openmarkov.inference.arcReversal.action;

import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.inference.arcReversal.ArcReversal;

import java.util.List;
//import org.openmarkov.inference.arcReversal.ArcReversal.InferencePurpose;
//import org.openmarkov.inference.arcReversal.ArcReversal.InferenceState;

@SuppressWarnings("serial")
/** Removes a chance node in an Arc Reversal algorithm.
 * This Edit class is valid for Bayesian networks and influence diagrams.
 * */
public class CRemoveChanceNodeAREdit extends CRemoveNodeAREdit {
	
	//InferencePurpose purpose,
	//,
	//InferenceState inferenceState
	public CRemoveChanceNodeAREdit(ProbNet probNet,
			List<TablePotential> constantPotentials,	 
			Variable variableToDelete, 
			ArcReversal arcReversal,
			boolean isLastVariable) {
		super(probNet,constantPotentials,variableToDelete,arcReversal,CRemoveChanceNodeAREdit.class,isLastVariable); // purpose, ,inferenceState
	
	}

	//InferencePurpose purpose, 
	//,
	//InferenceState inferenceState
    private CRemoveChanceNodeAREdit(ProbNet probNet,
                                    List<TablePotential> constantPotentials,
                                    Variable variableToDelete,
                                    ArcReversal arcReversal) {
		this(probNet,constantPotentials,variableToDelete,arcReversal,false); // purpose, ,inferenceState
	
	}


	/** @return <code>String</code> */
	public String toString() {
		return new String("CRemoveChanceNodeAREdit: " +	variable);
	}


	@Override
	protected TablePotential auxMarginalizePotentials(List<TablePotential> arrayListPotentials)
	 {
	     return DiscretePotentialOperations
                 .multiplyAndMarginalize(arrayListPotentials, variable);
	}


	

}
