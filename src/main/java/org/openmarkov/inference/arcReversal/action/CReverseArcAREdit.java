/*
 * Copyright 2013 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.arcReversal.action;

import org.openmarkov.core.action.BaseLinkEdit;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.model.graph.Link;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;

import java.util.ArrayList;
import java.util.List;

/** 
 * @author artasom
 *  
 * Inverts the arc between two nodes.
 * 
 * Being X -> Y the link that is going to be inverted and being:
 * A: the group of nodes that are parents of X and are not parents of Y,
 * C: the group of nodes that are parents of Y (except X) and are not parents of X, and
 * B the group of parents that X and Y share,
 * 
 * The process takes five steps:
 * 
 * 1. Invert the arc.
 * 
 * 2. Share parents between the nodes.
 * 
 * 3. 	Calculate P(x, y|a, b, c) through P(x, y|a, b, c) = P(x|a, b) · P(y|x, b, c)
 * 		Meaning: P(x, y|a, b, c) = pot(x) · pot(y)
 * 
 * 4. Calculate P(y|a, b, c) through P(y|a, b, c) = Σ(x) P(x, y|a, b, c) and assign to node Y this probability.
 * 
 * 5. Calculate P(x|a, b, c, y) through P(x|a, b, c, y) = P(x, y|a, b, c) / P(y|a, b, c) and assign to node X this probability. 
 */
@SuppressWarnings("serial")
public class CReverseArcAREdit extends BaseLinkEdit {

	// x (parent) node
    private final Node x;
	// y (child) node
    private final Node y;

	// Parent node's old potentials    
    private List<Potential> parentsOldPotentials;
	// Child node's old potentials
    private List<Potential> childsOldPotentials;

	// In case of undo, this list will keep the links created so they can be deleted
	private final List<Link> undoLinks = new ArrayList<>();

	// Constructor
	/** @param probNet <code>ProbNet</code>
	 * @param variable1 <code>Variable</code>
	 * @param variable2 <code>Variable</code>
	  */
    //* @param isDirected <code>boolean</code>
	public CReverseArcAREdit (ProbNet probNet,
			Variable variable1,
			Variable variable2)
	//boolean isDirected)
	{
		// It will always be directed
		super (probNet, variable1, variable2, true);
		x = probNet.getNode (variable1);
		y = probNet.getNode (variable2);

	}

	// Methods
	@Override
	/** @throws exception <code>Exception</code> */
	public void doEdit() throws DoEditException {

		// The parents of x are retrieved
		List<Node> xParents = x.getParents();
		// The parents of y are retrieved
		List<Node> yParents = y.getParents();
		// The nodes will share their parents
		List<Node> newParents;

		// 1. Invert the arc.
		// The link between i and j can be removed
		probNet.removeLink(x, y, true);
		// and the link between j and i can be created
		probNet.addLink(y, x, true);

		// 2. Share parents between the nodes.
		// The list of created links is emptied
		undoLinks.clear();
		// {C(x) \ C(y)} must be parents of y
		// The new parents of y will be those nodes that are parents of x,
		newParents = xParents;
		// and weren't already parents of y
		newParents.removeAll(yParents);

		// The new links are created 
		for (Node newParent : newParents) {
			// creating the Link is creating a Link in the node and thus in the graph
			undoLinks.add(new Link(newParent, y, true));
			//probNet.addLink(probNet.getNode(newParent), y, true);
		}

		// {C(y) \ C(x) \ x}
		// The new parents of x will be those nodes that are parents of y,
		newParents = yParents;
		// and weren't already parents of i
		newParents.removeAll(xParents);
		// excluding also the i node itself
		newParents.remove(x);

		// The new links are created
		for (Node newParent : newParents) {
			// creating the Link is creating a Link in the node and thus in the graph
			undoLinks.add(new Link(newParent, x, true));
			//probNet.addLink(probNet.getNode(newParent), x, true);
		}

		List<TablePotential> xyPotentials = new ArrayList<>();

		parentsOldPotentials = x.getPotentials();
		childsOldPotentials = y.getPotentials();

		// 3. 	Calculate P(x, y|a, b, c) through P(x, y|a, b, c) = P(x|a, b) · P(y|x, b, c)
		// Meaning: P(x, y|a, b, c) = pot(x) · pot(y)

		// pot(x) are added to xyPotentials 
		for (Potential parentsOldPotential : parentsOldPotentials) {
			xyPotentials.add((TablePotential) parentsOldPotential);
		}

		// pot(y) are added to xyPotentials 
		for (Potential childsOldPotential : childsOldPotentials) {
			xyPotentials.add((TablePotential) childsOldPotential);
		}

		// xyPotentials are multiplied
		TablePotential xyPotentialMultiplied = DiscretePotentialOperations.multiply(xyPotentials);

		// 4. Calculate P(y|a, b, c) through P(y|a, b, c) = Σ(x) P(x, y|a, b, c) and assign to node Y this probability.
		TablePotential yNewPotential = DiscretePotentialOperations.marginalize(xyPotentialMultiplied, x.getVariable());
		y.setPotential(yNewPotential);

		// 5. Calculate P(x|a, b, c, y) through P(x|a, b, c, y) = P(x, y|a, b, c) / P(y|a, b, c) and assign to node X this probability.	
        TablePotential xNewPotential = null;
        xNewPotential = DiscretePotentialOperations.divide(xyPotentialMultiplied, yNewPotential);
        x.setPotential(xNewPotential);

		for (Link link : undoLinks) {
            probNet.addLink((Node)link.getNode1(), (Node)link.getNode2(), true);
        }
	}




	public void undo() {
		super.undo();
		try {
			// Delete link Y -> X
			probNet.removeLink(variable2, variable1, isDirected);
			// Re-create link X -> Y
			probNet.addLink(variable1, variable2, isDirected);
			// Delete the links created when the nodes shared their fathers
			for (Link<Node> undoLink : undoLinks) {
				probNet.removeLink(undoLink.getNode1(), undoLink.getNode2(), true);
			}
			// The potentials of X are restored to the original ones
			x.setPotentials(parentsOldPotentials);
			// The potentials of Y are restored to the original ones
			y.setPotentials(childsOldPotentials);
		}
        catch (Exception exc) {
            exc.printStackTrace();
        }
	}

	/** Method to compare two InvertLinkEdits comparing the names of
	 * the source and destination variable alphabetically.
	 * @param obj
	 * @return
	 */
	public int compareTo(CReverseArcAREdit obj){
		int result;

		if ((result = variable1.getName().compareTo(obj.getVariable1().
				getName())) != 0)
			return result;
		if ((result = variable2.getName().compareTo(obj.getVariable2().
				getName())) != 0)
			return result;
		else
			return 0;
	}

	@Override
	public String getOperationName() {
		return "Invert";
	}

	/** This method assumes that the link is directed, otherwise has no sense.
	 * @return <code>String</code> */
	public String toString() {
		return "Reverse arc: " + variable1 + "-->" + variable2 +
                " ==> " + variable2 + "-->" + variable1;
	}

	@Override
	public BaseLinkEdit getUndoEdit ()
	{
		return new CReverseArcAREdit (getProbNet (), getVariable2 (), getVariable1 ()); //, isDirected () is always true in this case
	}

}