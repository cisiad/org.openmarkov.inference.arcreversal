/*
 * Copyright 2013 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.arcReversal;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.exception.InvalidStateException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.annotation.InferenceAnnotation;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNetOperations;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.constraint.UtilityNodes;
import org.openmarkov.core.model.network.constraint.NoSuperValueNode;
import org.openmarkov.core.model.network.constraint.OnlyDirectedLinks;
import org.openmarkov.core.model.network.constraint.NoCycle;
import org.openmarkov.core.model.network.potential.Intervention;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.arcReversal.action.CRemoveChanceNodeAREdit;
import org.openmarkov.inference.arcReversal.action.CRemoveDecisionNodeAREdit;
import org.openmarkov.inference.arcReversal.action.CRemoveNodeAREdit;
import org.openmarkov.inference.arcReversal.action.CReverseArcAREdit;

// import es.uned.cisiad.artaso.tfm.algorithmsComparison.tools.ACProfiling;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;


/**
 * <code>ArcReversal</code> algorithm for influence diagrams (hereinafter, ID).
 *
 * Based on the algorithm appeared in Shachter1986 & Tatman1990,
 *
 * Shachter's algorithm requires an oriented, regular ID An oriented ID is an ID
 * with only one value node, but here it will be adapted to deal with more than
 * one value node.
 *
 * The algorithm here presented does not take networks with super-value nodes.
 *
 *
 * @author artasom
 */

@InferenceAnnotation(name = "ArcReversal")
public class ArcReversal extends InferenceAlgorithm {

    private HashMap<Variable, String> strategy;
    private TablePotential globalUtility;

    /** Set of network types where the algorithm can be applied. */
    private List<NetworkType> networkTypesApplicable;

    private boolean probNetHasOnlyOneValueNode;

    private boolean isLastVariable;
    private int numVariablesToEliminate;
    private int numVariablesEliminated;

    // For profiling
    // ACProfiling acProfiling;

    /**
     * Set of additional constraints that the ProbNet must satisfy in
     * conjunction with the constraints typical of the network types. Its
     * initial value must be null, instead of an empty ArrayList<PNCconstraint>,
     * so that the method getRequiredConstraints of its child classes detect
     * when this property has not been initialized
     */
    private List<PNConstraint> additionalConstraints;
    private final boolean forceEliminationOrder = false;

    public ArcReversal(ProbNet probNet) throws NotEvaluableNetworkException {
        super(probNet);
        this.probNet = probNet.copy();
        pNESupport = this.probNet.getPNESupport();
        probNetHasOnlyOneValueNode = this.probNet.getNodes(NodeType.UTILITY).size()==1;
        isLastVariable = false;
        numVariablesEliminated = 0;
        setInferenceStateAndInitializeStructures(); // InferenceState.PRERESOLUTION
        setConditioningVariables(new ArrayList<Variable>());
        // @@@ Profiling - artasom master thesis
        // acProfiling = new ACProfiling("ArcReversal",this.probNet.getName());
    }




    /**
     * @return A new <code>ArrayList</code> of <code>PNConstraint</code>.
     */
    private static List<PNConstraint> initializeAdditionalConstraints() {
        List<PNConstraint> constraints;
        constraints = new ArrayList<>();
        // Necessary???
        // constraints.add(new NoMixedParents());
        // The ProbNet has to have at least one utility node
        constraints.add(new UtilityNodes());
        // The ProbNet has no super-value node
        constraints.add(new NoSuperValueNode());
        // Only directed links (necessary?)
        constraints.add(new OnlyDirectedLinks());
        // And no cycles
        constraints.add(new NoCycle());

        return constraints;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    private static List<NetworkType> initializeNetworkTypesApplicable() {
        List<NetworkType> networkTypes;
        networkTypes = new ArrayList<>();
        networkTypes.add(InfluenceDiagramType.getUniqueInstance());
        return networkTypes;
    }

    /**
     * @return An <code>ArrayList</code> of <code>NetworkType</code> where the
     *         algorithm can be applied: Bayesian networks and influence
     *         diagrams.
     */
    protected final List<NetworkType> getNetworkTypesApplicable() {

        if (networkTypesApplicable == null) {
            networkTypesApplicable = initializeNetworkTypesApplicable();
        }

        return networkTypesApplicable;
    }

    /**
     * @return The additional constraints. If it is null then it is initialized.
     */
    protected final Collection<PNConstraint> getAdditionalConstraints() {

        if (additionalConstraints == null) {
            additionalConstraints = initializeAdditionalConstraints();
        }

        return additionalConstraints;
    }

    public HashMap<Variable, String> getStrategy (){
        return this.strategy;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.openmarkov.core.inference.InferenceAlgorithm#isEvaluable(org.openmarkov
     * .core.model.network.ProbNet)
     */
    @Override
    public boolean isEvaluable(ProbNet probNet) {
        boolean isApplicable;

        // TODO Check global constraints for it can be called from API?

        List<NetworkType> networkTypes = initializeNetworkTypesApplicable();

        isApplicable = false;
        // Check that there is a network type applicable equal to type of
        // probNet
        for (int i = 0; (i < networkTypes.size()) && !isApplicable; i++) {
            isApplicable = (networkTypes.get(i) == probNet.getNetworkType());
        }

        // Check that the probNet satisfies the specific constraints of the
        // algorithm

        // Constrainst according to Shatcher's algorithm

        // It is said to be oriented if it contains a value node. In this case,
        // we will allow to have more than one value node, but not super-value
        // nodes.

        // (1) the directed graph has no cycles --> included in additionalConstraints
        // (necessary?) // OnlyDirectedLinks?

        if (isApplicable) {
            List<PNConstraint> additionalConstraints;
            additionalConstraints = initializeAdditionalConstraints();
            for (int i = 0; (i < additionalConstraints.size()) && isApplicable; i++) {
                isApplicable = additionalConstraints.get(i).checkProbNet(
                        probNet);
            }
        }


        // (2) the value node, if present, has no successors, and
        // This constraint is fulfilled by the ProbNet, isn't it?
        Iterator<Node> upnIterator = probNet.getNodes(NodeType.UTILITY).iterator();

        while (upnIterator.hasNext() && isApplicable) {
            isApplicable = (upnIterator.next().getChildren().size() == 0);
        }

        // TODO
        // (3) there is a directed path that contains all of the
        // decision nodes

        // Just with a topological check is not enough...
        // In Elvira was also checked that at least one decision node
        // was connected to the utility node. So far, it's not being checked.

		/*
		if (isApplicable) {
			isApplicable = pathBetweenDecisions();
		}
		 */

        return isApplicable;
    }

    @Override
    public Intervention getOptimalStrategy() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param globalUtility
     *            Sets the field 'globalUtility'
     */
    void setGlobalUtility(TablePotential globalUtility) {
        this.globalUtility = globalUtility;
    }

    private ProbNet pruneNetwork(ProbNet net) {


        // in order to use the same prune method the "utility" variable should
        // be considered "variableOfInterest", no?


        // this way we could avoid to modify the method "removeBarrenNodes" in
        // ProbNetOperations

        // In Variable Elimination this is not the case because
        // in order to prune the network a Markov network is used...
        // So the code can be changed here to use a Markov Network

        List<Variable> variablesOfInterest = net.getVariables(NodeType.UTILITY);

        EvidenceCase evidence = null;

        try {
            evidence = joinPreAndPostResolutionEvidence();
        } catch (IncompatibleEvidenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ProbNetOperations.getPruned(net, variablesOfInterest,evidence);
    }




    private EvidenceCase joinPreAndPostResolutionEvidence()
            throws IncompatibleEvidenceException {
        EvidenceCase evidence = new EvidenceCase(getPreResolutionEvidence());
        try {
            evidence.addFindings(getPostResolutionEvidence().getFindings());
        } catch (InvalidStateException e) {
            e.printStackTrace();
        }
        return evidence;
    }

    private void setInferenceStateAndInitializeStructures() {
        this.strategy = new HashMap<>();
        this.globalUtility = null;
    }

    @Override
    public Potential getOptimizedPolicy(Variable decisionVariable)
            throws IncompatibleEvidenceException, UnexpectedInferenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Potential getExpectedUtilities(Variable decisionVariable)
            throws IncompatibleEvidenceException, UnexpectedInferenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TablePotential getGlobalUtility()
            throws IncompatibleEvidenceException, UnexpectedInferenceException {
        try {
            //acProfiling.startProfiling();
            resolveNetwork();
            //acProfiling.stopProfiling();
        } catch (WrongCriterionException e) {
            e.printStackTrace();
        }
        return this.globalUtility;

    }

    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities()
            throws IncompatibleEvidenceException, UnexpectedInferenceException {


        List<TablePotential> projectedTablePotentials;
        EvidenceCase evidence = getPreResolutionEvidence();

        boolean needToCheckIfEvidenceIsCompatible;

        needToCheckIfEvidenceIsCompatible = evidence.getFindings().size() == this.probNet.getChanceAndDecisionVariables().size();

        if (needToCheckIfEvidenceIsCompatible) {
            checkIfEvidenceIsCompatible(this.probNet, evidence);
        }

        if (evidence != null && !evidence.isEmpty()) {
            try {
                projectedTablePotentials = this.probNet.tableProjectPotentials(evidence);
            } catch (NonProjectablePotentialException | WrongCriterionException e1) {
                throw new IncompatibleEvidenceException("Unexpected inference exception :" + e1.getMessage());
            }

            for (Potential potential : this.probNet.getPotentials()){
                this.probNet.removePotential(potential);
            }

            for (TablePotential projectedTablePotential : projectedTablePotentials) {
                this.probNet.addPotential(projectedTablePotential);
            }
        }

        for (Finding finding : evidence.getFindings()) {
            // variable of the finding
            Variable variable = finding.getVariable();
            // its node
            Node node = null;
            try {
                node = probNet.getNode(variable.getName());
            } catch (NodeNotFoundException e) {
                e.printStackTrace();
            }
            // remove the node form the probNet, as it is no longer neccesary
            probNet.removeNode(node);
            // if it is a decision, it will be part of the strategy
            if (node != null) {
                if (node.getNodeType() == NodeType.DECISION) {
                    this.strategy.put(variable, variable.getStateName(evidence.getState(variable)));
                }
            }
        }

        List<Variable> variablesOfInterest = this.probNet.getVariables();
        HashMap<Variable, TablePotential> individualProbabilities;
        ProbNet prunedNet;


        // After checking the prerequisites of the network,
        // Shachter's algorithms continues, first of all,
        // adding "no forgetting" arcs
        ProbNetOperations.addNoForgettingArcs(probNet);

        // and eliminating all barren nodes
        prunedNet = pruneNetwork(probNet);

        numVariablesToEliminate = probNet.getChanceAndDecisionVariables().size();

        // The method performInference is not correctly defined. As of now, the definition is taken from VariableElimination.
        // The method is only working in a pre-resolution state and only modifies the links in the probabilistic network,
        // it makes nothing with the potentials.

        // According to the algorithm, when only the utility node remains
        // "At that point, it has determined all of the optimal policies and computed the maximal
        // expected utility."
        individualProbabilities = performInference(prunedNet, variablesOfInterest, new EvidenceCase(getPreResolutionEvidence())); // InferencePurpose.POSTERIOR_PROB
        return individualProbabilities;
    }


    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities(List<Variable> variablesOfInterest)
            throws IncompatibleEvidenceException, UnexpectedInferenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TablePotential getJointProbability(List<Variable> variables)
            throws IncompatibleEvidenceException, UnexpectedInferenceException {
        // TODO Auto-generated method stub
        return null;
    }

	/*	*//**
     * To get the number of decisions in the diagram.
     *
     * @return the number of decisions.
     *//*

	private int numberOfDecisions() {
		// Return the number of decisions
		return this.probNet.getNodes(NodeType.DECISION).size();
	}*/

	/*	private List<Node> order(ProbNet pN) {
		// i is certain node
		// S(i) are the direct successors if i
		// S(i) = {j (pert.) N: (i, j) (pert.) A},

		// Take a node that belongs to M
		// which direct sucessors are not in M
		// In the first step, this means that
		// the algorithm is taking a node that has no
		// children.


	 * IF W != 0 THEN RETURN (APPEND (order(M\W),W)) ELSE IF M=0 THEN
	 * RETURN(W) ELSE ERROR ("Cycle in graph")


	 *//** A partial order is a list of lists of variables. *//*

		boolean nodeFound = false;
		List<Node> pnOrdered = new ArrayList<Node>();
		Iterator<Node> pnIterator = pN.getNodes().iterator();
		Node node = null;

		while (!nodeFound && pnIterator.hasNext()) {
			node = pnIterator.next();

			if (node.getNode().getNumChildren() == 0) {
				nodeFound = true;
			}
		}

		if (nodeFound) {
			pN.removeNode(node);
			pnOrdered.add(node);
			pnOrdered.addAll(order(pN));
		} else {
			if (pN.getNodes().size() != 0) {
				// cycle
				return null;
			}
		}

		return pnOrdered;

	}*/

	/*	*//**
     * Checks that there is a path between the decision nodes and the utility
     * node.
     *
     * @return <code>true</code> if OK, <code>false</code> in other case.
     *//*
	// Is this a possible new constraint???
	private boolean pathBetweenDecisions() {
		//Node lastDecision;

		//List<Node> decisionNodes = this.probNet
		//		.getNodes(NodeType.DECISION);
		List<Node> utilityNodes = this.probNet
				.getNodes(NodeType.UTILITY);

		boolean reachable = false;

		// Check if the utility node is accessible from
		// the last decision
		for (Node utilityNode : utilityNodes) {
			for (Node utilityNodeParent : utilityNode.getNode()
					.getParents()) {
				try {
					if (this.probNet.getNode(utilityNodeParent)
							.getNodeType() == NodeType.DECISION) {
						reachable = true;
						// make this better...
						return reachable;
					}
				} catch (NodeNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return reachable;
	}*/

	/*	// Methods created for CRemove...
	// TODO Auto-generated method stub
	 *//**
     * @param potential
     * @return True if and only if the potential has to be added to the list of
     *         constant potentials. It also detects 0 in probability potentials
     *         and then throws IncompatibleEvidenceException
     * @throws IncompatibleEvidenceException
     *//*
	public static boolean checkIfIncludeInMarkovDecisionNetwork(TablePotential potential) {

		return ((potential != null) && (potential.getVariables().size() > 0));

	}
	  */
	/*	// Methods created for CRemove...
	// TODO Auto-generated method stub
	 *//**
     * @param potential
     * @return True if and only if the potential has to be added to the list of
     *         constant potentials. It also detects 0 in probability potentials
     *         and then throws IncompatibleEvidenceException
     * @throws IncompatibleEvidenceException
     *//*
	public static boolean checkIfAddToConstantPotentials(TablePotential potential)
			throws IncompatibleEvidenceException {
		PotentialRole role;
		boolean addPotential;

		if (potential != null) {
			role = potential.getPotentialRole();
			addPotential = false;
			int sizeVariables = potential.getVariables().size();
			if (role != PotentialRole.UTILITY) {
				if ((sizeVariables == 0) && (potential.values[0] == 0.0)) {
					throw new IncompatibleEvidenceException("Incompatible Evidence");
				}
				addPotential = false;
			} else {
				addPotential = (sizeVariables == 0);
			}
		} else {
			addPotential = false;
		}
		return addPotential;

	}
	  */


    /**
     * Checks if the evidence is compatible with the network (structure and
     * potentials)
     *
     * @param probNet A probabilistic network
     * @param evidence Evidence being checked
     * @throws IncompatibleEvidenceException
     * @throws UnexpectedInferenceException
     */
    private void checkIfEvidenceIsCompatible(ProbNet probNet, EvidenceCase evidence)
            throws IncompatibleEvidenceException,
            UnexpectedInferenceException {

        // TODO Check
        /*
		HashMap<Variable, TablePotential> jointProbabilityOrUtility = getJointProbabilityOrUtility(probNet,
				evidence.getVariables(), new EvidenceCase()); //  InferencePurpose.POSTERIOR_PROB,

		if (jointProbabilityOrUtility.getValue(evidence) <= 0.0) {
			throw new IncompatibleEvidenceException(
					"The probability of the introduced evidence is 0.0");
		}
         */
    }

	/*	*//**
     * Prunes the <code>net</code> (which would probably has been pruned before
     * in the case of computation of marginal probabilities). After that, it
     * projects the evidence in the pruned net and eliminates the variables that
     * does not belong to <code>queryVariables</code> or to the
     * <code>evidence</code> variables. When this finishes the method multiplies
     * the remaining potentials and returns them.
     *
     * @param purpose
     * @param queryVariables
     *            <code>ArrayList</code> of <code>Variable</code>.
     * @param evidence
     *            <code>EvidenceCase</code>.
     * @return One potential with the <code>variablesOfInterest</code>
     *         probability table.
     * @throws NotEnoughMemoryException
     * @throws UnexpectedInferenceException
     * @throws IncompatibleEvidenceException
     *//*
	// InferencePurpose purpose,
	protected HashMap<Variable, TablePotential> getJointProbabilityOrUtility(ProbNet net, 
			List<Variable> queryVariables, EvidenceCase evidence)
					throws UnexpectedInferenceException,
					IncompatibleEvidenceException {

		//if (inferenceState == InferenceState.PRERESOLUTION) {
		try {
			resolveNetworkIfThereAreDecisionsWithoutPolicy();
		} catch (WrongCriterionException e) {
			e.printStackTrace();
		}
		//setInferenceStateAndInitializeStructures(InferenceState.POSTRESOLUTION);
		setInferenceStateAndInitializeStructures();
		//}

		HashMap<Variable, TablePotential> jointProbability = null;

		jointProbability = performInference(net, queryVariables, evidence); //purpose, 

		return jointProbability;
	}*/

	/*	*//**
     * resolves the network if it has some decision whose policy has to be
     * calculated.
     *
     * @throws IncompatibleEvidenceException
     * @throws NotEnoughMemoryException
     * @throws UnexpectedInferenceException
     *
     *//*
	private void resolveNetworkIfThereAreDecisionsWithoutPolicy() throws 
	WrongCriterionException, IncompatibleEvidenceException, UnexpectedInferenceException {

		boolean thereAreDecWithoutPolicy;
		List<Variable> decisions = probNet.getVariables(NodeType.DECISION);

		thereAreDecWithoutPolicy = false;

		for (int i = 0; (i < decisions.size()) && !thereAreDecWithoutPolicy; i++) {
			thereAreDecWithoutPolicy = !hasImposedPolicy(decisions.get(i));
		}

		if (thereAreDecWithoutPolicy) {
			resolveNetwork();
		}
	}*/


    /**
     * @throws IncompatibleEvidenceException
     *             Resolves the network: Computes the optimized policies and the
     *             global utility. The new state is POSTRESOLUTION.
     * @throws UnexpectedInferenceException
     */
    private void resolveNetwork() throws WrongCriterionException,
            IncompatibleEvidenceException, UnexpectedInferenceException {
        // Comment this call. Prepare the network and the perform inference... The global utility is being calculated and stored...
        getProbsAndUtilities();
    }


    /**
     * This method has been designed to be used in both states: PRERESOLUTION
     * and POSTRESOLUTION. It implements the basic variable elimination scheme.
     * In PRESOLUTION state it computes the optimized policies and the global
     * utility. In POSTRESOLUTION state it calculates the posterior
     * probabilities and utilities.
     *
     * @param network The probabilistic network in which inference is performed
     */
	/*	 
	   Shachter's algorithm preconditions
	   check for oriented (one value node) --> DONE (check global prerequisites)
	   regular influence diagram --> DONE (check decisions are ordered 
	            							and that directed path between decision nodes
	            							and path from last decision to value node)
		and "no forgetting" arcs --> DONE

		eliminate all barren nades --> DONE (here we are working with the pruned net
	 */
    // TO REMOVE A DECISION NODE, IT HAS TO HAVE AS ONLY SUCCESOR, ONE VALUE NODE IT CAN BE REMOVED BY MAX.
    // IF IT HAS MORE THAN ONE SUCCESOR AND ALL OF THEM ARE VALUE NODES, THEY CAN BE SUMMED UP... THIS FITS WITH THE
    // ALREADY PROGRAMMED ALGORITHM FOR VE...
    private HashMap<Variable, TablePotential> performInference(ProbNet network,
                                                               List<Variable> queryVariables, EvidenceCase evidence)
            throws UnexpectedInferenceException,
            IncompatibleEvidenceException {

        HashMap<Variable, TablePotential> individualProbabilities = new HashMap<>();

        ProbNet net = network.copy();
        ProbNet auxNetCheckingPaths;

        Node i;

        List<Node> auxChildren;
        Iterator<Node> auxChildrenIterator;

        Node auxNode1;
        Node auxNode2;

        String variableEliminationOrder = "";
        int arcReversalPositionType;
        //To store the name of the variable in order to create the string for the order elimination
        String variableName;

        // Finally, the utility node(s) is/are the one left in the network

        // The conditional predecessor of the utility nodes of the network are retrieved
        List<Node> utilityNodesConditionalPredecessors = getUtilityNodesConditionalPredecessors(net);
        try {
            // While the value node has conditional predecessors
            while (utilityNodesConditionalPredecessors.size() != 0) {
/*                //update probNet size
                if (acProfiling.getMeasureSize()) {
                    acProfiling.updateProbNetMaxSize(net);
                }*/
                // check if there exists a chance node that is conditional predecessor
                // of a value node and whose only successor is that value node
                i = getChanceNodeCPandOSValueNode(net);
                // if a node of these characteristics is found,
                if (i != null) {
                    //store the name
                    variableName = i.getName();
                    // then, node has to be removed
					/*
					Shachter theorem 1 
					node i may be removed by conditional expectation.
					Afterward, the value node inherits all of the conditional
					predecessors from node i, and thus the process creates
					no new barren nodes.
					 */
                    isLastVariable = numVariablesToEliminate - numVariablesEliminated == 1;
                    numVariablesEliminated++;
                    individualProbabilities.put(i.getVariable(),
                            eliminateVariable(i.getVariable(), isLastVariable, net, null)); // purpose,
                    // for force var elim order
                    arcReversalPositionType=0;
                }
                // If such a node is not found, a new kind of node must be looked for:
                else {
                    // A decision node that is a conditional predecessor of a value node
                    // and that the conditional predecessors of that value node
                    // are a proper subset of the union of that node and its informational predecessors
                    // (So that the chance nodes are known when the decisions are made...)
                    i = getDecisionNodeCPValueNodeAndCPSubsetIPuI(net);
                    // if a node of these characteristics is found,
                    if (i != null) {
                        //store the name
                        variableName = i.getName();
                        // Then, node has to be removed
                        // no need to take in count the need of making links between IP of
                        // the decision node and the value node because the value node
                        // is already linked to them (because of the no-forgetting-arcs, right?)
                        isLastVariable = numVariablesToEliminate - numVariablesEliminated == 1;
                        numVariablesEliminated++;
                        individualProbabilities.put(i.getVariable(),
                                eliminateVariable(i.getVariable(), isLastVariable, net, null)); // purpose,
                        // eliminate barren nodes
                        net = pruneNetwork(net);
                        // for force var elim order
                        arcReversalPositionType=1;
                    }
                    else {
                        // We look for a chance node that is:
                        //   - conditional predecessor of the utility node
                        //   - has not a decision node among its children
                        i = getChanceNodeCPValueNodeAndNoDecisionChildren(net);
                        //store the name
                        variableName = i.getName();
                        // while the node i has children that are chance nodes...
                        auxChildren = i.getChildren();
                        // @ 18/08/2013
                        // This part of the algorithm is not working when
                        // there exists a path from the node that is to be eliminated
                        // Change of the approach. Not to iterate over the children but
                        // to move through all of the till the children are empty
                        // ---
                        // we only have to take in count children that are chance nodes
                        for (auxChildrenIterator = auxChildren.iterator();auxChildrenIterator.hasNext();) {
                            if (auxChildrenIterator.next().getNodeType() != NodeType.CHANCE) {
                                auxChildrenIterator.remove();
                            }
                        }
                        //while (auxChildrenIterator.hasNext()) {
                        // There is no need to access the children randomly, as all of them have to be processed
                        while (auxChildren.size() > 0) {
                            //auxChildrenNode = auxChildrenIterator.next();

                            //if (net.getNode(auxChildrenNode).getNodeType() == NodeType.CHANCE) {
                            // fetch a chance node j that has no other directed path with i:
                            // As i and j are connected through a link, if this link is removed and no other path is found,
                            // the node j is found
                            auxNetCheckingPaths = net.copy();
                            // (Because the copy of the network doesn't make exact copies of the Nodes)
                            auxNode1 = auxNetCheckingPaths.getNode(i.getName());
                            //we get the first item of the list
                            //auxNode2 = auxNetCheckingPaths.getNode(net.getNode(auxChildrenNode).getName());
                            auxNode2 = auxNetCheckingPaths.getNode(auxChildren.get(0).getName());
                            auxNetCheckingPaths.removeLink(auxNode1, auxNode2, true);
                            if (!auxNetCheckingPaths.existsPath(auxNode1, auxNode2, true)) {
                                // The arc (i,j) can be reversed
                                reverseArc(net,i,auxChildren.get(0));
                                auxChildren.remove(0);
                            }
                            // if not, it's moved a position forward...
                            else {
								/*
									Node auxNode = auxChildren.get(0);
									auxChildren.remove(0);
									auxChildren.add(auxChildren.size(), auxNode);
								 */
                                Collections.rotate(auxChildren, 1);
                            }
                            //}

                        }
                        // after all these actions are taken, the node i can be removed
                        // but it has not to be a barren node, because it will be a conditional predecessor
                        // of the utility node
                        isLastVariable = numVariablesToEliminate - numVariablesEliminated == 1;
                        numVariablesEliminated++;
                        individualProbabilities.put(i.getVariable(),
                                eliminateVariable(i.getVariable(), isLastVariable, net, null)); // purpose,

                        // for force var elim order
                        arcReversalPositionType=2;
                    }
                }
/*                if (acProfiling.acConfiguration.getStoreVariableInferenceOrder()) {
                    variableEliminationOrder = variableEliminationOrder + variableName +
                            acProfiling.getSplitParticularRegularExpresion()
                            + "ARPosType=" + arcReversalPositionType +
                            acProfiling.getGeneralRegularExpresion();
                }*/
                // The network has changed, so the conditional predecessor of the utility nodes are updated
                utilityNodesConditionalPredecessors = getUtilityNodesConditionalPredecessors(net);
            }
        } catch (NodeNotFoundException e) {
            e.printStackTrace();
        }

        ////System.out.print(variableEliminationOrder);
        ////System.out.println("===========");

        // Finally, the utility node(s) is/are the one left in the network

        // if more than one utility node... SUM !


        List<Node> utilityNodes = net.getNodes(NodeType.UTILITY);
        List<TablePotential> utilityNodesTablePotentials = new ArrayList<>();

        for (Node utilityNode : utilityNodes ) {
            try {
                individualProbabilities.put(utilityNode.getVariable(),net.getNode(utilityNode.getVariable()).getUtilityFunction());
                for (Potential potential : utilityNode.getPotentials()) {
                    utilityNodesTablePotentials.add((TablePotential) potential);
                }
                //this.setGlobalUtility(net.getNode(utilityNode.getVariable()).getUtilityFunction());
            } catch (NonProjectablePotentialException | WrongCriterionException e) {
                e.printStackTrace();
            }
        }

        TablePotential sumUtilityPotentials = DiscretePotentialOperations.sum(utilityNodesTablePotentials);
        this.setGlobalUtility(sumUtilityPotentials); //= sum of utilities... ; no?


        //this.setGlobalUtility(utilityPotentialsList.get(0));
        //System.out.println(net);

/*        // if requested, the variable elimination order will be stored in the database
        if (acProfiling.acConfiguration.getStoreVariableInferenceOrder()) {
            // last pipe is erased
            variableEliminationOrder = variableEliminationOrder.substring(0, variableEliminationOrder.length() - 1);
            acProfiling.acConfiguration.setVariableInferenceOrder(this.probNet.getName(),
                    acProfiling.getInferenceAlgorithmID(),variableEliminationOrder);
        }*/

        return individualProbabilities;

    }


    private List<Node> getUtilityNodesConditionalPredecessors (ProbNet probNet) {
        List<Node> utilityNodes = probNet.getNodes(NodeType.UTILITY);
        List<Node> utilityNodesCP = new ArrayList<>();

        for (Node utilityNode : utilityNodes) {
            utilityNodesCP.addAll(utilityNode.getParents());
        }
        return utilityNodesCP;
    }

    private List<Node> getUtilityNodeConditionalPredecessors (Node utilityNode) {
        //List<Node> utilityNodes = probNet.getNodes(NodeType.UTILITY);
        List<Node> utilityNodeCP = new ArrayList<>();

        utilityNodeCP.addAll(utilityNode.getParents());

        return utilityNodeCP;
    }

    /**
     *
     * @param probNet The probabilistic network in use
     * @return A node that is a chance node and a conditional predecessor of a value node
     * 		   and has as only successor that same value node. If there is not a
     * 		   node with these characteristics, the methods returns null
     *
     * 			Note, 13/07/2013 -> or has as children only value nodes...
     *
     */
    private Node getChanceNodeCPandOSValueNode(ProbNet probNet) {
        Node node = null;
        Node auxNode;

        // in the firts moment, the node has not been found
        boolean nodeFound = false;
        ////boolean allChildrenValueNodes = true;

        // We fetch all the conditional predecessors of the utility nodes
        List<Node> utilityNodesConditionalPredecessors = getUtilityNodesConditionalPredecessors(probNet);

        int lowerBound = 0;
        int upperBound;
        int randomPosition;

        int posType = 0;
/*        if (acProfiling.getInferenceVariableEliminationOrder() != null && forceEliminationOrder) {
            node = acProfiling.checkVariableInferenceEliminationOrderAR(probNet, posType);
        }
        else {*/
            // We create an iterator on the conditional predecessors
            ////Iterator<Node> utilityNodesConditionalPredecessorsIterator = utilityNodesConditionalPredecessors.iterator();

            // The conditional predecessors are iterated, if the kind of node that is being looked for is not found
            //// while (utilityNodesConditionalPredecessorsIterator.hasNext() && !nodeFound) {

            // The conditional predecessors are iterated *randomly*, if the kind of node that is being looked for is not found
            while (!utilityNodesConditionalPredecessors.isEmpty() && !nodeFound) {
                ////auxNode = utilityNodesConditionalPredecessorsIterator.next();

                // The access to the nodes is made randomly. In order to achieve this,
                // a position to be visited is calculated randomy.
                upperBound = utilityNodesConditionalPredecessors.size() - 1;
                randomPosition = getIntegerInRange(lowerBound,upperBound);
                auxNode = utilityNodesConditionalPredecessors.get(randomPosition);

                // The auxiliary node is a chance node and a conditional predecessor (direct predecessor)
                // of a value node
                try {
                    if (auxNode.getNodeType() == NodeType.CHANCE) {
                        // and if it has only one children, this can only be that value node,
                        // so the kind of node that is being looked for is found
                        if (auxNode.getChildren().size() == 1) {
                            nodeFound = true;
                        }
                        // or if all the children are value nodes...
                        if (!nodeFound) {
                            nodeFound = allNodesSameType(auxNode.getChildren(), NodeType.UTILITY);
                        }
                        // if the node has been found, it is stored to be returned
                        if (nodeFound) {
                            node = auxNode;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // the element is removed form the list
                utilityNodesConditionalPredecessors.remove(randomPosition);
            }
        //}
        return node;
    }

    /**
     *
     * @param probNet The probabilistic network in use
     * @return A decision node that is a conditional predecessor of a value node
     *         and that the conditional predecessors of that value node
     *         are a proper subset of the union of that node and its informational predecessors
     *         If there is not a node with these characteristics, the methods returns null
     *
     *         Note, 13/07/2013 -> checking all the value nodes...
     *
     */
    private Node getDecisionNodeCPValueNodeAndCPSubsetIPuI(ProbNet probNet) {

        Node node = null;
        Node auxNode;
        Node auxUtilityNode;

        boolean nodeFound = false;
        boolean removable;

        List<Node> informationalPredecessorAndDecisionNode = new ArrayList<>();

        List<Node> utilityNodes = probNet.getNodes(NodeType.UTILITY);

        int lowerBound = 0;
        int upperBound;
        int randomPosition;
        int predeccessorLowerBound = 0;
        int predeccessorUpperBound;
        int predeccessorRandomPosition;

        int posType = 1;

/*        if (acProfiling.getInferenceVariableEliminationOrder() != null && forceEliminationOrder) {
            node = acProfiling.checkVariableInferenceEliminationOrderAR(probNet, posType);
        }

        else {*/
            // Taken from Tatman1990
            // Although it requires all the value nodes to be non negative...
            // See theorems 4 and 5
            // If a value node is the only successor, max.
            // if more than one, sum and max (after checking other constraints -> review)
            // The utility nodes are iterated, if the kind of node that is being looked for is not found
            //// while (utilityNodesIterator.hasNext() && !nodeFound) {
            // The utility nodes are iterated *randomly*, if the kind of node that is being looked for is not found
            while (!utilityNodes.isEmpty() && !nodeFound) {

                ////auxUtilityNode = utilityNodesIterator.next();

                // The access to the nodes is made randomly. In order to achieve this,
                // a position to be visited is calculated randomly.
                upperBound = utilityNodes.size() - 1;
                randomPosition = getIntegerInRange(lowerBound,upperBound);
                auxUtilityNode = utilityNodes.get(randomPosition);

                // We fetch all the conditional predecessors of a utility node
                List<Node> utilityNodeConditionalPredecessors = getUtilityNodeConditionalPredecessors(auxUtilityNode);


                List<Node> auxUtilityNodeConditionalPredecessors;
                List<Node> aux2UtilityNodeConditionalPredecessors;
                // We create an iterator on the conditional predecessor of that utility node
                ////Iterator<Node> utilityNodeConditionalPredecesorsIterator = utilityNodeConditionalPredecessors.iterator();

                // The conditional predecessors are iterated, if the kind of node that is being looked for is not found
                ////while (utilityNodeConditionalPredecesorsIterator.hasNext() && !nodeFound) {

                auxUtilityNodeConditionalPredecessors = new ArrayList<>(utilityNodeConditionalPredecessors);

                while (!utilityNodeConditionalPredecessors.isEmpty() && !nodeFound) {

                    // To access them randomly, we get the upper bound, the upper possible position
                    predeccessorUpperBound = utilityNodeConditionalPredecessors.size() - 1;
                    // get the random position
                    predeccessorRandomPosition = getIntegerInRange(predeccessorLowerBound,predeccessorUpperBound);
                    ////auxNode = utilityNodeConditionalPredecesorsIterator.next();
                    auxNode = utilityNodeConditionalPredecessors.get(predeccessorRandomPosition);

                    try {
                        // If the auxiliary node is a decision node
                        // and the conditional predecessors of the utility node are a proper subset (or subset?) of
                        // its informational predecessors (its direct predecessors, so its parents)
                        // and the node itself,
                        // the kind of node that is being looked for is found
                        if (auxNode.getNodeType() == NodeType.DECISION) {
                            // in each iteration, the informational predecessors of the decision node
                            // must be clear

                            // ??? getInformationalPredecessors from VariableElimination !

                            informationalPredecessorAndDecisionNode.clear();
                            informationalPredecessorAndDecisionNode.addAll(auxNode.getParents());
                            informationalPredecessorAndDecisionNode.add(auxNode);


                            // If the utility node has siblings and this are utility nodes, the informational predeccesors of them are neccesary too
                            if (auxNode.getChildren().size() > 1) {
                                for (Node auxNodeChildren : auxNode.getChildren()) {
                                    try {
                                        if (auxNodeChildren.getNodeType() == NodeType.UTILITY &&
                                                auxNodeChildren.getName().compareTo(auxUtilityNode.getName()) != 0) {
                                            aux2UtilityNodeConditionalPredecessors = getUtilityNodeConditionalPredecessors(auxNodeChildren);
                                            aux2UtilityNodeConditionalPredecessors.removeAll(auxUtilityNodeConditionalPredecessors);
                                            auxUtilityNodeConditionalPredecessors.addAll(aux2UtilityNodeConditionalPredecessors);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            if (informationalPredecessorAndDecisionNode.containsAll(auxUtilityNodeConditionalPredecessors) &&
                                    auxUtilityNodeConditionalPredecessors.size() <= informationalPredecessorAndDecisionNode.size()) {
                                // if the network has only one value node, the node is potentially removable
                                // explain...
                                removable = this.probNetHasOnlyOneValueNode || auxNode.getChildren().size() == 1 || allNodesSameType(auxNode.getChildren(), NodeType.UTILITY);
                                // if the node is removable...
                                if (removable) {
                                    nodeFound = true;
                                    node = auxNode;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // if not, the element is deleted from the list
                    utilityNodeConditionalPredecessors.remove(predeccessorRandomPosition);
                }
                // the element is removed, if the node has not been found
                utilityNodes.remove(randomPosition);


            }
        //}
        // the result (a node found or null) is returned
        return node;
    }

    /**
     *
     * @param probNet The probabilistic network in use
     * @return A chance node that is a conditional predecessor of a utility node
     * 		   and has not a decision node among its children.
     *         If there is not a node with these characteristics, the methods returns null.
     */
    private Node getChanceNodeCPValueNodeAndNoDecisionChildren(ProbNet probNet) {
        Node node = null;
        Node auxNode;

        boolean nodeFound = false;
        boolean allAuxNodeChildrenNoDecisionNodes;

        Iterator<Node> auxNodeChildrenIterator;

        // We fetch all the conditional predecessors of the utility nodes
        List<Node> utilityNodesConditionalPredecessors = getUtilityNodesConditionalPredecessors(probNet);

        int lowerBound = 0;
        int upperBound;
        int randomPosition;


        int posType = 2;

/*        if (acProfiling.getInferenceVariableEliminationOrder() != null && forceEliminationOrder) {
            node = acProfiling.checkVariableInferenceEliminationOrderAR(probNet,posType);
        } else {*/

            // We create an iterator on the conditional predecessors
            ////Iterator<Node> utilityNodesConditionalPredecessorsIterator = utilityNodesConditionalPredecessors.iterator();

            // The conditional predecessors are iterated, if the kind of node that is being looked for is not found
            ////while (utilityNodesConditionalPredecessorsIterator.hasNext() && !nodeFound) {
            while (!utilityNodesConditionalPredecessors.isEmpty() && !nodeFound) {

                // If the auxiliary node is a chance node and a conditional predecessor
                // and among its children, there are no decision node,
                // then the kind of node that is being looked for is found

                ////auxNode = utilityNodesConditionalPredecessorsIterator.next();

                // The access to the nodes is made randomly. In order to achieve this,
                // a position to be visited is calculated randomly.
                upperBound = utilityNodesConditionalPredecessors.size() - 1;
                randomPosition = getIntegerInRange(lowerBound,upperBound);
                auxNode = utilityNodesConditionalPredecessors.get(randomPosition);


                if (auxNode.getNodeType() == NodeType.CHANCE) {
                    auxNodeChildrenIterator = auxNode.getChildren().iterator();
                    allAuxNodeChildrenNoDecisionNodes = true;
                    while (auxNodeChildrenIterator.hasNext() && allAuxNodeChildrenNoDecisionNodes) {
                        if (auxNodeChildrenIterator.next().getNodeType() == NodeType.DECISION) {
                            allAuxNodeChildrenNoDecisionNodes = false;
                        }
                    }
                    if (allAuxNodeChildrenNoDecisionNodes) {
                        nodeFound = true;
                        node = auxNode;
                    }
                }

                // the element is removed form the list
                utilityNodesConditionalPredecessors.remove(randomPosition);
            }
        //}
        // the result (a node found or null) is returned
        return node;
    }


    private boolean allNodesSameType (List<Node> nodes, NodeType type) {
        boolean allSameType = true;
        Iterator<Node> nodesIterator = nodes.iterator();
        while (allSameType && nodesIterator.hasNext()) {
            try {
                allSameType = nodesIterator.next().getNodeType() == type;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return allSameType;
    }


    private TablePotential reverseArc(ProbNet probNet,Node i,Node j) {

        TablePotential posteriorUtil = null;

        CReverseArcAREdit edit = new CReverseArcAREdit(probNet, i.getVariable(), j.getVariable());

        try {
            probNet.getPNESupport().doEdit(edit);
        } catch (DoEditException | NonProjectablePotentialException | WrongCriterionException e) {
            e.printStackTrace();
        }

        return posteriorUtil;

    }


    //			ProbNet markovNetworkInference
    // InferencePurpose purpose,
    private TablePotential eliminateVariable(Variable variableToDelete,
                                             boolean isLastVariable,
                                             ProbNet probNet,
                                             List<TablePotential> constantPotentials)
            throws IncompatibleEvidenceException {

        NodeType nodeType;
        CRemoveNodeAREdit edit = null;
        String policy;

        nodeType = probNet.getNode(variableToDelete).getNodeType();

        switch (nodeType) {
            case CHANCE:
                edit = new CRemoveChanceNodeAREdit(probNet, constantPotentials,
                        variableToDelete, this, isLastVariable); // purpose, , inferenceState
                break;
            case DECISION:
                edit = new CRemoveDecisionNodeAREdit(probNet, constantPotentials,
                        variableToDelete, this, isLastVariable); // purpose, , inferenceState
                break;
            default:
                break;
        }

        try {
            probNet.getPNESupport().doEdit(edit);
        } catch (DoEditException | NonProjectablePotentialException | WrongCriterionException e) {
            e.printStackTrace();
        }

        if (nodeType == NodeType.DECISION) {
            //TablePotential policy = edit.getPolicy();
            //if (policy != null) {
            policy = edit != null ? edit.getPolicy() : null;
            if (policy == null) {
                EvidenceCase evidence = new EvidenceCase(getPreResolutionEvidence());
                //policy = variableToDelete.getName() + "=" + variableToDelete.getStateName(evidence.getState(variableToDelete));
                policy = variableToDelete.getStateName(evidence.getState(variableToDelete));
            }
            this.strategy.put(variableToDelete, policy);
            //}
        }
        TablePotential posteriorUtilOrPotential;
        //if (purpose!=InferencePurpose.EXPECTED_UTIL){
        //storeGlobalUtilityIfNecessary(isLastVariable, edit);
        //posteriorUtilOrPotential = getPosteriorUtilityIfNecessary(isLastVariable, edit); // , purpose
        //}

        posteriorUtilOrPotential = edit.getNormalizedProbabilityPotential();

        return posteriorUtilOrPotential;

    }


	/*	private void storeGlobalUtilityIfNecessary(boolean isLastVariable, CRemoveNodeAREdit edit) {
		if (isLastVariable) {
			setGlobalUtility(edit.getGlobalUtility());
		}
	}*/

    public static void checkEvaluability(ProbNet probNet) throws NotEvaluableNetworkException {
        boolean isApplicable;

        List<NetworkType> networkTypes = initializeNetworkTypesApplicable();

        isApplicable = false;
        NetworkType networkType = probNet.getNetworkType();
        // Check that there is a network type applicable equal to type of
        // probNet
        for (int i = 0; (i < networkTypes.size()) && !isApplicable; i++) {
            NetworkType auxType = networkTypes.get(i);

            isApplicable = (auxType == networkType);
        }

        if (!isApplicable){
            throw new NotEvaluableNetworkException("Network type "+networkType.toString()+"is not evaluable.");
        }
        // If this point is reached, the algorithm is, so far, applicable.
        // Check that the probNet satisfies the specific constraints of the
        // algorithm
        List<PNConstraint> additionalConstraints = initializeAdditionalConstraints();
        for (int i = 0; (i < additionalConstraints.size()); i++) {
            PNConstraint pnConstraint = additionalConstraints.get(i);
            if (!pnConstraint.checkProbNet(probNet)){
                throw new NotEvaluableNetworkException("Constraint "+pnConstraint.toString()+" is not satisfied by the network.");
            }

        }
    }

    private int getIntegerInRange (int lowerBound, int upperBound) {
        return (lowerBound + (int)(Math.random() * ((upperBound - lowerBound) + 1)));
    }

}
